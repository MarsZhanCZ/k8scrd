package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type PrewarmType string

const (
	T1 PrewarmType = "InUse"
	T2 PrewarmType = "Directional"
	// Phase I we do not support directional prewarm
	T3 PrewarmType = "Undirectional"
	T4 PrewarmType = "Stopped"
)

type PrewarmPool struct {
	Type PrewarmType `json:"prewarmType,omitempty"`
	// The type of prewarm pool
	TenantProjectId string `json:"projectId,omitempty"`
	// The tenant id, for T1&T2 only
	TenantDomainId string `json:"domainId,omitempty"`
	// The tenant id, for T1&T2 only
	Namespace string `json:"namespace,omitempty"`
	// The namespace where the vm is in, for T1&T2 only
	SalePolicy string `json:"salePolicy,omitempty"`
	// The sale types, e.g., common, high performance, etc.
	DedicatedSize string `json:"dedicatedSize,omitempty"`
	// The dedicated pod size
}

type StateMachine string

const (
	En StateMachine = "Entering"
	Av StateMachine = "Available"
	Oc StateMachine = "Occupied"
	Re StateMachine = "Recycling"
	// Phase I we do not support recycle
	Le StateMachine = "Leaving"
)

type ResourceName string

const (
	ResourceCPU              ResourceName = "cpu"
	ResourceMemory           ResourceName = "memory"
	ResourceStorage          ResourceName = "storage"
	ResourceEphemeralStorage ResourceName = "ephemeral-storage"
	ResourceStorageLocalDir  ResourceName = "localdir"
	ResourceLocalVolume      ResourceName = "localvolume"
	ResourceLocalSSD         ResourceName = "localssd"
	ResourceDefault          ResourceName = "localauto"
)

type NodeHealth struct {
	IsHealthy          bool        `json:"isHealthy,omitempty"`
	IsPodRunning       bool        `json:"isPodRunning,omitempty"`
	LastTransitionTime metav1.Time `json:"lastTransitionTime,omitempty"`
	Maintainer         string      `json:"maintainer,omitempty"`
}

type VmNodeStatus struct {
	BelongedPool PrewarmPool `json:"belongedPool,omitempty"`
	//
	State StateMachine `json:"state,omitempty"`
	// The state of the vm node
	Reason string `json:"reason,omitempty"`

	TransitTime metav1.Time `json:"transitTime,omitempty"`

	Health NodeHealth `json:"health,omitempty"`
}

type VmNodeSpec struct {
	VmId string `json:"vmid,omitempty"`
	// The vm id from ecs
	Name string `json:"name,omitempty"`
	Job  string `json:"job,omitempty"`
	// The ECS creation job
	Flavor string `json:"flavor,omitempty"`
	// The flavor id of the vm
	FailureDomain string `json:"failureDomain,omitempty"`
	// The information of failure domain, e.g., the host id
	CellId string `json:"cellId,omitempty"`
	// The cell of the vm node
	CreationTime metav1.Time `json:"creationTime,omitempty"`
	// The timestampe of creating this vm
}

// +genclient
// +k8s:client-gen=true
// +k8s:list-gen=true
// +k8s:informer-gen=true
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
type VmNode struct {
	metav1.TypeMeta `json:",inline"`

	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec VmNodeSpec `json:"spec,omitempty"`

	Status VmNodeStatus `json:"status,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
type VmNodeList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`

	Items []VmNode `json:"items,omitempty"`
}
