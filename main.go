package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"path/filepath"
	"time"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/util/homedir"

	"github.com/k8sCrd/pkg/apis/marszhancz.dev/v1alpha1"
	klient "github.com/k8sCrd/pkg/client/clientset/versioned"
	kInfFac "github.com/k8sCrd/pkg/client/informers/externalversions"
)

func main() {
	//k := v1alpha1.Kluster{}

	fmt.Println("creating clientset......")
	klusterCache := KlusterCache{VmNodes: make(map[string]*v1alpha1.VmNode)}

	var kubeconfig *string

	if home := homedir.HomeDir(); home != "" {
		kubeconfig = flag.String("kubeconfig", filepath.Join(home, ".kube", "config"), "(optional) absolute path to the kubeconfig file")
	} else {
		kubeconfig = flag.String("kubeconfig", "", "absolute path to the kubeconfig file")
	}
	flag.Parse()

	config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
	if err != nil {
		log.Printf("Building config from flags failed, %s, trying to build inclusterconfig", err.Error())
		config, err = rest.InClusterConfig()
		if err != nil {
			log.Printf("error %s building inclusterconfig", err.Error())
		}
	}

	klientset, err := klient.NewForConfig(config)
	if err != nil {
		log.Printf("getting klient set %s\n", err.Error())
	}
	fmt.Println(klientset)

	fmt.Println("creating informer......")
	// 和informer分离的lister。是直接访问apiserver的，并不是使用informer缓存的。这个不好。
	// klusterList, err := klientset.MarszhanczV1alpha1().VmNodes("").List(context.Background(), metav1.ListOptions{})
	// if err != nil {
	// 	log.Printf("listing vmNodes %s\n", err.Error())
	// }
	// fmt.Printf("length of vmNodes is %d\n", len(klusterList.Items))

	// 要想使用和informer使用同一块内部缓存的lister，需要通过创建SharedIndexInformer对象，从中获取lister
	// 启动Lister之前必须启动informer
	infoFactory := kInfFac.NewSharedInformerFactory(klientset, time.Minute)
	klusterInformer := infoFactory.Marszhancz().V1alpha1().VmNodes().Informer()
	ch := make(chan struct{})

	fmt.Println("create lister from informer......")
	//go klusterInformer.Run(ch)
	klusterLister := infoFactory.Marszhancz().V1alpha1().VmNodes().Lister()

	// 初始化一个indexer。相当于是建立索引。每个vmNode被加入的时候都会根据所有的indexer增加索引。就可以快速从cache中取出了。
	fmt.Println("init an indexer on the Spec.Flavor")
	klusterIndexer := klusterInformer.GetIndexer()
	klusterIndexer.AddIndexers(cache.Indexers{
		"flavor": func(obj interface{}) ([]string, error) {
			vn := obj.(*v1alpha1.VmNode)
			return []string{vn.Spec.Flavor}, nil
		},
	})

	fmt.Println("starting informer......")
	infoFactory.Start(ch)

	fmt.Println("informer sync current cluster info......")

	// two ways of sync cache. either way is ok.
	// 注意，WaitForCacheSync是不会调用addFunc的，是直接更新cache。
	// 如果没有在一开始sync，就会根据resync的时间设置来判断是否会在informer开始后sync。
	// 如果为0则一直不会sync，否则会像resync那样调用ListWatch来实现sync。这还是会调用addFunc
	// 如果只注释掉了AddEventHandler里为所有元素触发addFunc，你会发现还是会调用addFunc。

	// 1. use cache.WaitForCacheSync
	// if !klusterInformer.HasSynced() {
	// 	fmt.Println("Waiting for informer to sync")
	// 	if ok := cache.WaitForCacheSync(ch, klusterInformer.HasSynced); !ok {
	// 		fmt.Println("failed to wait for caches to sync")
	// 	}
	// }

	// 2. use informerFactory.sync to sync info to its own cache.
	infoFactory.WaitForCacheSync(ch)
	fmt.Println("informer sync finished.")

	fmt.Println("informer addEventHandler......")
	// this registers the call back functions of informer
	// the informer will trigger these functions if
	// the corresponding event is detected from api-server
	//var isRegistering = true
	klusterInformer.AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: func(obj interface{}) {
			// if isRegistering {
			// 	return
			// }
			kluster := obj.(*v1alpha1.VmNode)
			klusterCache.VmNodes[kluster.ObjectMeta.Name] = kluster
			fmt.Printf("Node added: %s\n", kluster.ObjectMeta.Name)
		},
		UpdateFunc: func(oldObj, newObj interface{}) {
			oldkluster := oldObj.(*v1alpha1.VmNode)
			newkluster := newObj.(*v1alpha1.VmNode)
			delete(klusterCache.VmNodes, oldkluster.Spec.Name)
			klusterCache.VmNodes[newkluster.ObjectMeta.Name] = newkluster
			fmt.Printf("Node updated: %s\n", newkluster.ObjectMeta.Name)
		},
		DeleteFunc: func(obj interface{}) {
			kluster := obj.(*v1alpha1.VmNode)
			delete(klusterCache.VmNodes, kluster.ObjectMeta.Name)
			fmt.Printf("Node deleted: %s\n", kluster.ObjectMeta.Name)
		},
	})
	// sleep some time to let informer to finish register eventHandler and
	// let all the existing items go through the addFunc.
	// the invoke of addFunc is very light-weight and inevitable when register
	// the addFunc.
	//time.Sleep(1 * time.Millisecond)
	//isRegistering = false

	//indexer to check informer cache, to see if lister has added existing nodes..
	fmt.Println("lister get full info......")
	vmNodesList, _ := klusterLister.List(labels.Everything())
	fmt.Printf("Informer.Lister: length of vmNodes is %d\n", len(vmNodesList))

	vmNodesList, _ = klusterLister.List(labels.Everything())
	for _, aVmNode := range vmNodesList {
		key, _ := cache.MetaNamespaceKeyFunc(aVmNode)
		if item, ok, _ := klusterIndexer.GetByKey(key); ok {
			fmt.Printf("vmNode with key %s already exists in informer's cache\n", key)
			i := item.(*v1alpha1.VmNode)
			fmt.Printf("item has name: %s\n", i.ObjectMeta.Name)
		}
	}

	fmt.Println("try let indexer to find vmNodes that satisfy certain condition. Here is Spec.Flavor == s6large")
	vmNodesListFromIndex, _ := klusterIndexer.ByIndex("flavor", "s6large")
	for _, vn := range vmNodesListFromIndex {
		v := vn.(*v1alpha1.VmNode)
		fmt.Printf("Satisying condition vmNode is: %s\n", v.ObjectMeta.Name)
	}

	fmt.Println("indexer find finished.")

	go tryCRUD(*klientset)

	// endless waiting for stopCh signal. 等待Node Informer事件
	select {}
}

type KlusterCache struct {
	VmNodes map[string]*v1alpha1.VmNode
}

func tryCRUD(klientset klient.Clientset) {
	//try CRUD
	//create
	time.Sleep(4 * time.Second)
	fmt.Println("====================start CRUD=======================")
	fmt.Println("======================create=========================")
	nodeNumber := "111"
	vmnodeName := fmt.Sprintf("%s%s", "example-vmnode-", nodeNumber)
	vmNode_1 := NewVmNode(nodeNumber)
	fmt.Printf("now creating a vmNode with name example-vmnode-%s............\n", nodeNumber)
	time.Sleep(time.Second * 2)
	res1, _ := klientset.MarszhanczV1alpha1().VmNodes("default").Create(context.Background(), vmNode_1, metav1.CreateOptions{})
	fmt.Printf("now vmNode %s has Spec.VmId = %s\n", res1.ObjectMeta.Name, res1.Spec.VmId)

	klusterList, _ := klientset.MarszhanczV1alpha1().VmNodes("").List(context.Background(), metav1.ListOptions{})
	fmt.Printf("length of vmNodes is %d\n", len(klusterList.Items))

	//update
	fmt.Println("======================update=========================")
	newVmId := "vmId-12345"
	fmt.Printf("old vmNode.Spec.VmId is %s, update with %s\n", res1.Spec.VmId, newVmId)
	fmt.Printf("now update a vmNode with name example-vmnode-%s........\n", nodeNumber)
	time.Sleep(time.Second * 2)
	res1.Spec.VmId = newVmId
	res2, _ := klientset.MarszhanczV1alpha1().VmNodes("default").Update(context.Background(), res1, metav1.UpdateOptions{})
	fmt.Printf("now vmNode %s has Spec.VmId = %s\n", res2.ObjectMeta.Name, res2.Spec.VmId)

	klusterList, _ = klientset.MarszhanczV1alpha1().VmNodes("").List(context.Background(), metav1.ListOptions{})
	fmt.Printf("length of vmNodes is %d\n", len(klusterList.Items))

	//delete
	fmt.Println("======================delete=========================")
	fmt.Printf("now delete a vmNode with name example-vmnode-%s........\n", nodeNumber)
	err := klientset.MarszhanczV1alpha1().VmNodes("default").Delete(context.Background(), vmnodeName, metav1.DeleteOptions{})
	if err != nil {
		log.Printf("deleting error %s\n", err.Error())
	}
	time.Sleep(time.Second * 2)

	klusterList, _ = klientset.MarszhanczV1alpha1().VmNodes("").List(context.Background(), metav1.ListOptions{})
	fmt.Printf("length of vmNodes is %d\n", len(klusterList.Items))
}

func NewVmNode(number string) *v1alpha1.VmNode {
	vmnodeName := fmt.Sprintf("%s%s", "example-vmnode-", number)
	vmnode := &v1alpha1.VmNode{
		ObjectMeta: metav1.ObjectMeta{
			Name: vmnodeName,
		},
		Spec: v1alpha1.VmNodeSpec{
			Name:          "example-vmnode",
			VmId:          "Empty",
			Flavor:        "s6small",
			Job:           "ecs-12345",
			CellId:        "host-12345",
			FailureDomain: "zone-a",
			CreationTime:  metav1.Time{Time: time.Now()},
		},
	}
	return vmnode
}
