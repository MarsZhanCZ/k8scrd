Thanks a lot to:
https://www.youtube.com/watch?v=89PdRvRUcPU

3 main parts of a CRD:
-typemeta
-objmeta
-spec

type.go用来定义新的CRD
register用于将type注册到k8s，使用code-generator

generate:
-deepCopy Objects
-clientset
-informer
-lister

generate的参数可以通过flag，或者doc.go的开头使用注释进行global tag
或者在types里的某一个struct顶上进行locall tag。

chengzhengzhan@A200149438:~/k8sCrd$ tree
.
├── README.md
├── go.mod
├── go.sum
├── main.go
└── pkg
    └── apis
        └── marszhancz.dev
            └── v1alpha1
                ├── doc.go
                ├── register.go
                └── types.go

4 directories, 7 files

到这一步，文件情况是这样（types里没有deepCopy哦）

# 进行安装
$ go install ./cmd/{client-gen,deepcopy-gen,informer-gen,lister-gen}

# 获取GOPATH路径
$ go env | grep GOPATH
GOPATH="/Users/Christian/go"

# 查看
ls $GOPATH/bin
client-gen     deepcopy-gen   goimports      lister-gen
controller-gen defaulter-gen  informer-gen   type-scaffold


execDir=~/sdk/go/src/k8s.io/code-generator
# 运行 code-generator/generate-group.sh
"${execDir}"/generate-groups.sh all \
# 指定 group 和 version，生成deeplycopy以及client
github.com/k8sCrd/pkg/client github.com/k8sCrd/pkg/apis MarsZhanCZ:v1alpha1 \
# 指定头文件
--go-header-file="${execDir}"/hack/boilerplate.go.txt 
# 指定输出位置，默认为GOPATH
--output-base ../

execDir=~/sdk/go/src/k8s.io/code-generator

@Deprecated
"${execDir}"/generate-groups.sh all github.com/k8sCrd/pkg/client github.com/k8sCrd/pkg/apis marszhancz.dev:v1alpha1 --go-header-file="${execDir}"/hack/boilerplate.go.txt 

@Deprecated
"${execDir}"/generate-groups.sh all ./github.com/k8sCrd/pkg/client ./pkg/apis marszhancz.dev:v1alpha1 --go-header-file="${execDir}"/hack/boilerplate.go.txt --output-base ../

@Deprecated
"${execDir}"/generate-groups.sh all ./github.com/k8sCrd/pkg/client(这段开始是GoPath/src/) ./pkg/apis(这段是当前目录) marszhancz.dev:v1alpha1 --go-header-file="${execDir}"/hack/boilerplate.go.txt 

-v 10 可以输出log。

Generating deepcopy funcs
Generating clientset for crd.example.com:v1 at operator-test/pkg/client/clientset
Generating listers for crd.example.com:v1 at operator-test/pkg/client/listers
Generating informers for cr kubectl create -f manifests/vmNodeOne.yamld.example.com:v1 at operator-test/pkg/client/informers


doc.go
// +k8s:informers-gen=package

成功的命令：
../../k8s.io/code-generator/generate-groups.sh all     github.com/k8sCrd/pkg/client github.com/k8sCrd/pkg/apis     marszhancz.dev:v1alpha1     --go-header-file="${execDir}"/hack/boilerplate.go.txt -v 10

在这种情况下，我的项目在$GOPATH/src/github.com/k8sCrd/下。在go.mod里面我的module名是github.com/k8sCrd。而且注意，这个是在k8sCrd项目目录下执行的，此时GOMOD是指向k8sCrd的地址。

controller-gen paths=./pkg/apis/marszhancz.dev/v1alpha1 crd:crdVersions=v1 output:crd:artifacts:config=manifests


kubectl create -f manifests/marszhancz.dev_vmnodes.yaml

kubectl create -f manifests/vmNodeOne.yaml

kubectl delete vmNode example-vmnode-1

把
/home/chengzhengzhan/sdk/go/pkg/mod/k8s.io/client-go@v0.27.1/tools/cache/shared_informer.go
（即生成的informer）里的 AddEventHandlerWithResyncPeriod()的line 619-629注释掉，就不会在addEventHandler的时候给所有元素调用addFunc了。
而此时之前的waitForSync已经获取过一次当前的信息，将已有元素存入informer.cache里了。也就是说注释掉addEventHandler的给所有元素调用addFunc不会影响cache的全量
接下来就可以监听增量了。

code-generator version: release-1.27
controller-gen version: v0.11.3

输出示例：
chengzhengzhan@A200149438:~/sdk/go/src/github.com/k8sCrd$ ./k8sCrd
creating clientset......
&{0xc0003ff4a0 0xc00040e460}
creating informer......
create lister from informer......
init an indexer on the Spec.Flavor
starting informer......
informer sync current cluster info......
informer sync finished.
informer addEventHandler......
lister get full info......
Informer.Lister: length of vmNodes is 3
vmNode with key default/example-vmnode already exists in informer's cache
item has name: example-vmnode
vmNode with key default/example-vmnode-2 already exists in informer's cache
item has name: example-vmnode-2
vmNode with key default/example-vmnode-3 already exists in informer's cache
item has name: example-vmnode-3
try let indexer to find vmNodes that satisfy certain condition. Here is Spec.Flavor == s6large
Satisying condition vmNode is: example-vmnode-3
indexer find finished.
====================start CRUD=======================
======================create=========================
now creating a vmNode with name example-vmnode-111............
now vmNode example-vmnode-111 has Spec.VmId = Empty
Node added: example-vmnode-111
length of vmNodes is 4
======================update=========================
old vmNode.Spec.VmId is Empty, update with vmId-12345
now update a vmNode with name example-vmnode-111........
Node updated: example-vmnode-111
now vmNode example-vmnode-111 has Spec.VmId = vmId-12345
length of vmNodes is 4
======================delete=========================
now delete a vmNode with name example-vmnode-111........
Node deleted: example-vmnode-111
length of vmNodes is 3
Node updated: example-vmnode-2
Node updated: example-vmnode-3
Node updated: example-vmnode
Node updated: example-vmnode
Node updated: example-vmnode-2
Node updated: example-vmnode-3
Node updated: example-vmnode
Node updated: example-vmnode-2
Node updated: example-vmnode-3
